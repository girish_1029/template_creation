var name1 = document.getElementById("name");
var aadharid = document.getElementById("aadharId");
var tenure = document.getElementById("tenure");
var form1 = document.getElementById("form");
var new1 = document.getElementById("button");
var error1 = document.getElementById("error");
var table = document.getElementById("table");
var vd = document.getElementById("tableData");
var submitInfo = document.getElementById("submitInfo");
var tenureInMonths=document.getElementById("tenureInMonths");
var financialType=document.getElementById("financialType");
form1.addEventListener('submit', (event) => {
    event.preventDefault();
    console.log("i am in event listner");
    formValidator = 0;
    var formValidator = 0;
    console.log(name1.value);
    if (name1.value.length == 0 || aadharid.value.length == 0 || tenure.value.length == 0) {
        error1.innerHTML = "fields cannot be empty";
        formValidator = 1;
    }
    else if (isNaN(aadharid.value)) {
        error1.innerHTML = "aadhar id cannot contain characters";
        formValidator = 1;
    }
    else if (isNaN(tenure.value)) {
        error1.innerHTML = "tenure cannot contain characters";
        formValidator = 1;
    }
    submitInfo.innerHTML = "details submitted succeed";
    //getting json from api using fetch.
    //TODO do something here to show user that form is being submitted
    if (formValidator == 0) {
        fetch(event.target.action, {
            method: 'POST',
            body: new URLSearchParams(new FormData(event.target)) // event.target is the form
        }).then((resp) => {

            return resp.json(); // or resp.text() or whatever the server sends
        }).then((body) => {
            //used to convert a json to string
        }).catch((error) => {
            console.error(error);
        });
    }
});


var jsonData;
//templates dropdown
const templatesTag = document.getElementById("insertingTemplates");
var templatesURL = "http://127.0.0.1:8000/alltemplates";
fetch(templatesURL)
    .then(templatesResponse => {
        return templatesResponse.json();
    })
    .then(templatesResponse => {
        console.log(templatesResponse);
        console.log(templatesResponse["allTemplates"].length);
        jsonData=templatesResponse;
        for (var i = 0; i < templatesResponse["allTemplates"].length; i++) {
            const createElement = document.createElement('option');

            const text = document.createTextNode(templatesResponse["allTemplates"][i]["templateName"]);
            createElement.append(text);
            templatesTag.append(createElement);
        }
    })
// prefetching the valuies after changing dropdown
templatesTag.addEventListener('change',e=>{
    e.preventDefault();
    var a=templatesTag.value;
    console.log(templatesTag.value);
    console.log(jsonData["allTemplates"].length);

    for (var i = 0; i < jsonData["allTemplates"].length; i++) {
        if(a===jsonData["allTemplates"][i]["templateName"]){
            tenure.value=jsonData["allTemplates"][i]["tenure"];
            tenureInMonths.value=jsonData["allTemplates"][i]["tenureInMonths"]
            financialType.value=jsonData["allTemplates"][i]["financialType"];
            break;
        }
    }

})


