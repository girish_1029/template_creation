console.log("debugging in custom templates.js");
var createButton = document.getElementById("create");
var updateButton = document.getElementById("update");
var templateName = document.getElementById("templateName");
var tenure = document.getElementById("tenure");
var deleteButton = document.getElementById("deleteTemplateButton");
var selectTag = document.getElementById("deleteTemplate");
var tenureInMonths = document.getElementById("tenureInMonths");
var updateTagForTenureInMonths = document.getElementById("updateTagForTenureInMonths");
var selectFinancialType = document.getElementById("selectFinancialType");
var forUpdation=document.getElementById("forUpdation");
//clearing all options from dropdown
function removeOptions(selectElement,s) {
    let i, L = selectElement.options.length - 1;
    for(i = L; i >= 0; i--) {
        if(selectElement[i].value===s){
            selectElement.remove(i);
            break;
        }
    }
}
//adding options dynamically
function addOptions(forUpdation,Value){
    const createElement = document.createElement('option');
    const createElement2=document.createElement('option');
    createElement.append(Value);
    createElement2.append(Value);
    selectTag.append(createElement);
    forUpdation.append(createElement2);



}
// adding templates in database

createButton.addEventListener('click', e => {
    e.preventDefault();
    console.log(typeof (templateName.value));
    console.log(typeof (tenure.value));
    let url = "http://127.0.0.1:8000/submitTemplate";
    data = '{"templateName":"' + templateName.value +
        '","tenure":"' + tenure.value + '","tenureInMonths":"' + tenureInMonths.value + '","financialType":"' + selectFinancialType.value + '"}';

    console.log(data);
    params = {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: data
    }
    fetch(url, params).then(response => {
        console.log("in the fetch");
        return (response.json())
    }).then(data=>{
        addOptions(forUpdation,templateName.value);
    })
    

})

var templatesURL = "http://127.0.0.1:8000/alltemplates";
fetch(templatesURL)
    .then(templatesResponse => {
        return templatesResponse.json();
    })
    .then(templatesResponse => {
        console.log(templatesResponse);
        console.log(templatesResponse["allTemplates"].length);
        jsonData = templatesResponse;
        for (let i = 0; i < templatesResponse["allTemplates"].length; i++) {
            const createElement = document.createElement('option');

            const text = document.createTextNode(templatesResponse["allTemplates"][i]["templateName"]);
            createElement.append(text);
            selectTag.append(createElement);
        }
    })
deleteButton.addEventListener('click', e => {
    e.preventDefault();
    let url = "http://127.0.0.1:8000/deleteTemplate/" + selectTag.value;
    fetch(url, { method: 'DELETE' })
        .then(response => {
            return response.json();
        })
        .then(response => {
            console.log("in the fetch");
            console.log(response);
            console.log("below the fetch");
            removeOptions(forUpdation,selectTag.value);
            removeOptions(selectTag,selectTag.value);
            // let templateURL = "http://127.0.0.1:8000/alltemplates";
            // fetch(templateURL)
            //     .then(templatesResponse => {
            //         return templatesResponse.json();
            //     })
            //     .then(templatesResponse => {
            //         console.log(templatesResponse);
            //         console.log(templatesResponse["allTemplates"].length);
            //         jsonData = templatesResponse;
            //         for (let i = 0; i < templatesResponse["allTemplates"].length; i++) {
            //             const createElement = document.createElement('option');
            //             const text = document.createTextNode(templatesResponse["allTemplates"][i]["templateName"]);
            //             createElement.append(text);
            //             selectTag.append(createElement);
            //         }
            //     })
        })

})




//updation code
var selectTagForUpdation = document.getElementById("forUpdation");
var updateTagForTenure = document.getElementById("updateTagForTenure");
var templateData;
var templatesURL = "http://127.0.0.1:8000/alltemplates";
fetch(templatesURL)
    .then(templatesResponse => {
        return templatesResponse.json();
    })
    .then(templatesResponse => {
        console.log(templatesResponse);
        console.log(templatesResponse["allTemplates"].length);
        templateData = templatesResponse;
        for (var i = 0; i < templatesResponse["allTemplates"].length; i++) {
            const createElement = document.createElement('option');
            const text = document.createTextNode(templatesResponse["allTemplates"][i]["templateName"]);
            createElement.append(text);
            selectTagForUpdation.append(createElement);
        }

    })
var dropdownForUpdation = document.getElementById("forUpdation");
dropdownForUpdation.addEventListener('change', e => {
    var a = selectTagForUpdation.value;
    for (var i = 0; i < templateData["allTemplates"].length; i++) {
        if (a === templateData["allTemplates"][i]["templateName"]) {
            updateTagForTenure.value = templateData["allTemplates"][i]["tenure"];
            break;
        }
    }
})




updateButton.addEventListener('click', e => {
    url = "http://127.0.0.1:8000/updateTemplate" + "/" + selectTagForUpdation.value + "/" + updateTagForTenure.value + "/" + updateTagForTenureInMonths.value;
    params = {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        //body: data
    }
    fetch(url, params).then(response => {
        return (response.json())
    }).then(data => {
        console.log(data);
    })
})