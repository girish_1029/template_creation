from fastapi import FastAPI, Request, Form
from pydantic import BaseModel
from fastapi.templating import Jinja2Templates
import pymongo
import datetime
from fastapi.middleware.cors import CORSMiddleware
import random
class TemplateModel(BaseModel):
    templateName:str
    tenure:int
    tenureInMonths:int
    financialType:str
client = pymongo.MongoClient("mongodb://localhost:27017")
db = client["Database"]
userData = db["userDetails"]
templatesInfo = db['templatesInfo']
app = FastAPI()
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],  # methods like get,put,post,delete
    allow_headers=["*"],
)
templates = Jinja2Templates(directory="html")
templates_js = Jinja2Templates(directory="html")


@app.post("/submitform")
async def login(name: str = Form(...), aadharId: int = Form(...), tenure: int = Form(...),tenureInMonths:int=Form(...),financialType:str=Form(...)):
    print(name)
    print(aadharId)
    print(tenure)
    randomNumber = random.random()
    ts = datetime.datetime.now()
    l = userData.find()
    userData.insert_one({"name": name, "aadhar id": aadharId, "tenure": tenure,
                 "time": ts, "AggregateId": userData.find().count()+1,"tenureInMonths":tenureInMonths,"financialType":financialType})
    return {"post": "success"}


@app.get("/viewFormData")
async def getView():
    print("debugging in view form data in main.py")
    l1 = list(userData.find({}, {'_id': 0}))
    return {"body": l1}


# create
@app.post("/submitTemplate")
async def submitTemplate(templatemodel:TemplateModel):
    print("in the submitTemplate")
    print(templatemodel.templateName)
    if(len(list(templatesInfo.find({"templateName": templatemodel.templateName})))) == 0:
        templatesInfo.insert_one(
            ({"templateName": templatemodel.templateName, "tenure": templatemodel.tenure,"tenureInMonths":templatemodel.tenureInMonths,"financialType":templatemodel.financialType}))
        return {"message": "added successfully"}
    else:
        return {"message": "already exist"}
#delete
@app.delete("/deleteTemplate/{templateName}")
async def deleteTemplate(templateName:str):
    print("in the delete")
    if templatesInfo.find_one({"templateName":templateName})!=None:
        templatesInfo.delete_one({"templateName":templateName})
        return {"message":"deleted successfully"}
    else:
        return {"message":"templateName not found"}
#update
@app.put("/updateTemplate/{templateName}/{tenure}/{tenureInMonths}")
async def updateTemplate(templateName:str,tenure:int,tenureInMonths:int):
    print("debugging in update template main.py")
    d=templatesInfo.find({"templateName":templateName})
    templatesInfo.update_one({'templateName':templateName},{"$set":{'tenure':tenure,'tenureInMonths':tenureInMonths}})
    return({"message":"updated successfully"})
#read
@app.get("/getTemplate/{templateName}")
async def getTemplate(templateName:str):
    if templatesInfo.find_one({"templateName":templateName})!=None:
        print(list(templatesInfo.find({"templateName":templateName})))
        l=list(templatesInfo.find({"templateName":templateName}))
        tenure=l[0]["tenure"]
        return {"message":tenure}
    else:
        return{"message":"not exists"}

#get number of templates in data base
@app.get("/numberoftemplates")
async def numberOfTemplates():
    print("debugging in numberoftemplates")
    return {"size":templatesInfo.count()}



#getting all the templates from database
@app.get("/alltemplates")
async def allTemplates():
    l=list(templatesInfo.find({},{'_id':0}))
    print(l)
    return {"allTemplates":l}
